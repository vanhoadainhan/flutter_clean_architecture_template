import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_template/configs/app_configs.dart';
import 'package:flutter_clean_architecture_template/configs/app_flavor.dart';
import 'package:flutter_clean_architecture_template/l10n/localization_service.dart';
import 'package:flutter_clean_architecture_template/src/app/app.dart';

void main() async {
  await configMain(Flavor.dev);
  runApp(
    EasyLocalization(
      supportedLocales: LocalizationService.supportedLocales,
      path: LocalizationService.pathLocale,
      fallbackLocale: LocalizationService.supportedLocales[0],
      child: const MyApp(),
    ),
  );
}
