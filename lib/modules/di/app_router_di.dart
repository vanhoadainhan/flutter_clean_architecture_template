import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/routes/app_route/app_routes.dart';

@module
abstract class RouterInjection {
  @singleton
  AppRouter router() => AppRouter(navigationKey: navigationKey);
}

final navigationKey = GlobalKey<NavigatorState>(debugLabel: 'myKey');
