import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_template/src/base/domain/navigation/app_navigation.dart';
import 'package:flutter_clean_architecture_template/src/base/domain/navigation/app_popup_info.dart';

abstract class BasePopupInfoMapper {
  Widget map(AppPopupInfo appPopupInfo, AppNavigator navigator);
}
