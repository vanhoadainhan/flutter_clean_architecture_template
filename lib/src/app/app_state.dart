part of 'app_cubit.dart';

@freezed
class AppState with _$AppState {
  const factory AppState({
    @Default(ThemeMode.system) ThemeMode themeMode,
    @Default(LanguageType.vietnamese) LanguageType languageType,
  }) = _AppState;
}
