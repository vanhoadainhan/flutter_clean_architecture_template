import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/base/presentation/bloc/base_cubit.dart';
import 'package:flutter_clean_architecture_template/src/base/presentation/common_cubit/common_state.dart';

@injectable
class CommonCubit extends BaseCubit<CommonState> {
  CommonCubit() : super(const CommonState());

  void showLoading() {
    emit(state.copyWith(isLoading: true));
  }

  void hideLoading() {
    emit(state.copyWith(isLoading: false));
  }
}
