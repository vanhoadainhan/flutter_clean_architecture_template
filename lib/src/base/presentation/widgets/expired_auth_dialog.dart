import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_template/src/base/presentation/theme/app_theme_src.dart';

class ExpiredAuthDialog extends StatelessWidget {
  const ExpiredAuthDialog({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'notify'.tr(),
          style: AppTextStyle.textStyle14.copyWith(fontSize: 16, color: AppColor.black),
        ),
        const SizedBox(height: 8),
        Text(
          'expiredAuthDialogContent'.tr(),
          style: AppTextStyle.textStyle14,
        ),
      ],
    );
  }
}