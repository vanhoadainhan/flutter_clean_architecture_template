import 'package:flutter_clean_architecture_template/src/base/presentation/resources/asset_paths/asset_path.dart';

class ImagePaths {
  static String get imgLogo => _getImagePath('img_logo.png');

  static String _getImagePath(String imageName) {
    return AssetsPaths.images + imageName;
  }
}
