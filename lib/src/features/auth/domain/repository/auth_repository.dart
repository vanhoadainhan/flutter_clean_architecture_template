import 'package:flutter_clean_architecture_template/src/features/auth/domain/entity/login_input.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/use_case/login_use_case.dart';

abstract class AuthRepository {
  Future<bool> isLoggedIn();

  Future<LoginOutput> login(
    LoginInput input,
  );
}
