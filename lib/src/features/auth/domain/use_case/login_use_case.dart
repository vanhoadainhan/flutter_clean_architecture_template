import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/base/data/models/base_response.dart';
import 'package:flutter_clean_architecture_template/src/base/data/models/result.dart';
import 'package:flutter_clean_architecture_template/src/base/domain/base_use_case.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/data/data_source/models/login_model.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/entity/login_input.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/repository/auth_repository.dart';

typedef LoginOutput = Result<BaseResponse<List<LoginModel>>, Exception>;

@injectable
class LoginUseCase extends UseCase<LoginInput, LoginOutput> {
  LoginUseCase(this._repository);

  final AuthRepository _repository;

  @override
  Future<LoginOutput> execute(LoginInput input) async {
    final res = await _repository.login(input);
    return res;
  }
}
