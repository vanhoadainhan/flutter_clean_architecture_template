import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/base/data/data_source/local/local_storage_manager.dart';
import 'package:flutter_clean_architecture_template/src/base/data/models/result.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/data/data_source/network/auth_datasource.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/entity/login_input.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/repository/auth_repository.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/use_case/login_use_case.dart';

@Injectable(as: AuthRepository)
class AuthRepositoryImpl extends AuthRepository {
  AuthRepositoryImpl(this._networkDatasource, this._localStorage);

  final AuthDatasource _networkDatasource;
  final LocalStorageManager _localStorage;

  @override
  Future<LoginOutput> login(LoginInput input) async {
    try {
      final response = await _networkDatasource.login(input);

      return Success(response);
    } on Exception catch (e, stackTrace) {
      return Failure(
        e,
        stackTrace: stackTrace,
      );
    }
  }

  @override
  Future<bool> isLoggedIn() async {
    final isLoggedIn = await _localStorage.read(LocalStorageKey.token);
    return isLoggedIn != null;
  }
}
