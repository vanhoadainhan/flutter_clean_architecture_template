import 'package:dio/dio.dart';
import 'package:flutter_clean_architecture_template/configs/app_flavor.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/data/data_source/network/end_point.dart';
import 'package:retrofit/retrofit.dart';
import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/base/data/models/base_response.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/data/data_source/models/login_model.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/entity/login_input.dart';

part 'auth_datasource.g.dart';

abstract class AuthDatasource {
  Future<BaseResponse<List<LoginModel>>> login(LoginInput input);
}

@Injectable(as: AuthDatasource)
@RestApi()
abstract class AuthDatasourceImpl implements AuthDatasource {
  @factoryMethod
  factory AuthDatasourceImpl(Dio dio) => _AuthDatasourceImpl(dio);

  @override
  @POST(loginPath)
  Future<BaseResponse<List<LoginModel>>> login(@Body() LoginInput input);
}
