import 'package:injectable/injectable.dart';
import 'package:flutter_clean_architecture_template/src/base/presentation/bloc/base_cubit.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/entity/login_input.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/domain/use_case/login_use_case.dart';
import 'package:flutter_clean_architecture_template/src/features/auth/presentation/login/bloc/login_state.dart';
import 'package:flutter_clean_architecture_template/src/routes/app_route/app_routes.dart';

@lazySingleton
class LoginCubit extends BaseCubit<LoginState> {
  LoginCubit(this._useCase) : super(const LoginState());

  final LoginUseCase _useCase;

  Future<void> login() async {
    consumeState(
      () => _useCase.execute(
        LoginInput('username', 'password'),
      ),
      onSuccess: (successData) {
        navigation.replace(HomeRoute());
      },
    );
  }
}
