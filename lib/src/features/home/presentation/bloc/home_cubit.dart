import 'package:flutter_clean_architecture_template/src/base/presentation/bloc/base_cubit.dart';
import 'package:flutter_clean_architecture_template/src/features/home/presentation/bloc/home_state.dart';

class HomeCubit extends BaseCubit<HomeState> {
  HomeCubit() : super(HomeState());
}
