import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_clean_architecture_template/src/base/presentation/base_page_state.dart';
import 'package:flutter_clean_architecture_template/src/features/home/presentation/bloc/home_cubit.dart';

@RoutePage()
class HomeScreen extends BasePageState<HomeCubit> {
  HomeScreen({super.key});

  @override
  Widget buildPage(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Home page'),
      ),
    );
  }
}
