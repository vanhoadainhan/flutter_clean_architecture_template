import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:flutter_clean_architecture_template/configs/app_flavor.dart';
import 'package:flutter_clean_architecture_template/modules/bloc_observer/observer.dart';
import 'package:flutter_clean_architecture_template/modules/di/di.dart';
import 'package:path_provider/path_provider.dart';

Future<void> configMain(Flavor flavor) async {
  // Preserve splash screen until authentication complete.
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();
  await SystemChrome.setPreferredOrientations(
    [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
  );
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.dark,
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );
  final FlavorConfig flavorConfig = await FlavorConfig.setup(flavor);
  getIt.registerSingleton(flavorConfig);
  configureDependencies();

  // Set bloc observer and hydrated bloc storage.
  Bloc.observer = Observer();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: await getApplicationDocumentsDirectory(),
  );
}
